* Qt

Pour ceux qui développent avec Qt, la fonction ~generate-qt-includes~ appelle un petit morceau de shell qui essaie de générer la liste des entêtes nécessaires aux types Qt utilisés. Sans doute pas parfait (tente régulièrement d'inclure QStringLiteral), mais mieux que rien !

#+BEGIN_SRC emacs-lisp :tangle yes
  (defun generate-qt-includes ()
      "Insert a list of Qt includes matching Qt types found in this buffer"
	  (interactive)
	  (shell-command-on-region
	   (point-min) (point-max)
	   ;; Pourquoi ne peut-on pas mettre le pipe en début de ligne ?
	   "sed 's/\#.*include.*<.*>/#include <header>/' |
  sed 's://.*:// comment:' |
  sed -n 's/.*\\(Q[A-Z][a-zA-Z]*\\).*/#include <\\1>/p' |
  sort | uniq" )
    (insert-buffer "*Shell Command Output*"))
#+END_SRC

Le raccourci {{{META-(#)}}} appelle ~generate-qt-includes~

#+BEGIN_SRC emacs-lisp :tangle yes
  (global-set-key (kbd "M-#") 'generate-qt-includes)
#+END_SRC

#+BEGIN_SRC emacs-lisp :tangle yes :exports none
  (last-step-duration "Qt")
#+END_SRC

