* My Require

Un wrapper pour ~require~, qui n'empêche pas le chargement du ~.emacs~ lorsqu'un module n'est pas disponible, et génère des traces dans ~*Messages*~, avec en cas de succès, le temps passé par Emacs pour charger le module.

#+BEGIN_SRC emacs-lisp :tangle yes
  (defun my-require(feature)
    (condition-case nil
        (let ((ts (current-time)))
          (progn
            (require feature)
            (let ((elapsed (float-time (time-subtract (current-time) ts))))
              (message "Successfully load '%s' in %.3fs" feature elapsed))))
      (file-error
       (progn (message "Fail to load required feature '%s'" feature) nil))))
#+END_SRC

Il s'utilise ainsi :

#+BEGIN_SRC emacs-lisp :tangle no
  (when (my-require 'package-xxx)
    (message "Do foo")
    (message "Do bar")
    (message "Do baz"))
#+END_SRC

~my-require~ est largement utilisé dans la suite de ce fichier, mais afin de ne pas avoir partout un niveau d'indentation, le ~when~ est sous-entendu, et n'est pas exporté dans le HTML. Le code apparait donc ainsi :

#+BEGIN_SRC emacs-lisp :tangle no
  (my-require 'package-xxx)
  (message "Do foo")
  (message "Do bar")
  (message "Do baz")
#+END_SRC

C'est-à-dire comme avec un ~require~ classique. Exemple de traces :

#+BEGIN_EXAMPLE
Fail to load required feature ’p4’
Successfully load ’rainbow-mode’ in 0.031s
#+END_EXAMPLE

On peut aussi utiliser le /soft require/, qui ne génère pas d'erreur quand le module n'est pas trouvé :

#+BEGIN_SRC emacs-lisp :tangle no
  (require 'package-foobar nil t)
#+END_SRC

#+BEGIN_SRC emacs-lisp :tangle yes :exports none
  (last-step-duration "Require")
#+END_SRC
